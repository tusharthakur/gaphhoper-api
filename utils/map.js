exports.sortRoutes=(routes)=>{
    return routes.sort((a, b) => parseFloat(a.distance) - parseFloat(b.distance));
}