const { mapValidation } = require("../validators");
const { getRouteData } = require("./map.service");
const mapUtils = require("../utils/map");

exports.getRouteDirections = async (req, res) => {
	try {
		const { from, to, vehicle } = req.query;
		try {
			await mapValidation.routeDirectionValidation.validateAsync(req.query);
		} catch (error) {
			return res.status(400).json({ error: error.details[0].message });
		}
		let routesData
		try{

			routesData = await getRouteData(from, to, vehicle);
		}
		catch(error){
			console.log(error.error)
			return res.status(422).json({error:error?.response?.data?.message||"error"});
		}
			
		const paths=routesData?.data?.paths||[];
		let routes = paths.map((path) => ({
			distance:path.distance,
			distanceKMs: (path.distance / 1000).toFixed(2),
			time:path.time,
			timeMinutes: (path.time / 1000 / 60).toFixed(2),
			points: path.points,
			instructions: path.instructions,
		}));

		routes =mapUtils.sortRoutes(routes);

		return res.status(200).json({ routes });
	} catch (error) {
		console.log(error);
		res.status(500).json({ error: "Internal server error" });
	}
};
