const express = require("express");
const api = express.Router();
const { getRouteDirections, checkPointProximity } = require("./map.controller");

api.route("/route").get(getRouteDirections)

module.exports = api;