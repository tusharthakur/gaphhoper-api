require("dotenv").config();
const { default: axios } = require("axios");

const GRAPHOPPER_BASE_URL= process.env.GRAPHOPPER_BASE_URL;
const GRAPHOPPER_API_KEY= process.env.GRAPHOPPER_API_KEY;

exports.getRouteData = async (from, to, vehicle)=> {
    try{
	    const url = `${GRAPHOPPER_BASE_URL}/route?point=${from}&point=${to}&vehicle=${vehicle}&algorithm=alternative_route&key=${GRAPHOPPER_API_KEY}`;
        const response = await axios.get(url);
        return Promise.resolve(response);
    }catch(error){
        return Promise.reject(error);
    }
}