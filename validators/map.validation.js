const Joi = require("joi");

exports.routeDirectionValidation = Joi.object({
	from: Joi.string().required()
		.error((errors) => {
			errors.forEach((err) => {
				switch (err.code) {
					case "any.required":
						err.message = "Invalid Input in from.";
						break;
					default:
						break;
				}
			});
			return errors;
		}),
	to: Joi.string().required()
		.error((errors) => {
			errors.forEach((err) => {
				switch (err.code) {
					case "any.required":
						err.message = "Invalid Input in to.";
						break;
					default:
						break;
				}
			});
			return errors;
		}),
	vehicle: Joi.string().required()
		.error((errors) => {
			errors.forEach((err) => {
				switch (err.code) {
					case "any.required":
						err.message = "Invalid Input in vehicle.";
						break;
					default:
						break;
				}
			});
			return errors;
		}),
});
