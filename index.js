require("dotenv").config();
const express = require("express");
const app = express();
const PORT = process.env.PORT;

const helmet = require("helmet");
const morgan = require("morgan");
const cors = require("cors");
const { MapRoutes } = require("./Map")

app.use(express.json());
app.use(helmet());
app.use(morgan("dev"));
app.use(cors());

app.get('/health', async (req, res) => {
	return res.status(200).json({message: "ok"});
})

app.use("/", MapRoutes);

app.use("*", (req, res) => {
	res.status(404).json({ message: "Route not found" });
});

app.use((error, req, res, next) => {
	console.log(error);
	res.status(500).json({ message: "Internal server error" });
});

app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}`);
});
